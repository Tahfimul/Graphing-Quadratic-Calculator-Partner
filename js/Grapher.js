var canvas = document.getElementById("canvas"), 
c = canvas.getContext("2d"), 

// n is the number of line segments.
n=1000,
//define the main window.
xMin = -12,
xMax =  12,
yMin = -12,
yMax =  12,
//end of main window
//Stroke Colors
colors = ['black'], 

color,


//Graph
math = mathjs(),
expr = '',
scope = { x : 0,  t : 0},
tree = math.parse(expr, scope),
// End of graph

//Time
time = 0,
timeIncrement = .01,
//End of Time
cLine;
// The main program
textField();
startAnimation();
// End of main program

function drawCurve(){
var i,xPixel,yPixel,

// these vary between 0 and 1 
percentX,percentY,
//these are in math coordinates
mathX, mathY;

//Clear the canvas
c.clearRect(0 ,0,canvas.width, canvas.height);

c.beginPath();

color = colors[0];
colors.push(color);

for(i=0;i<n;i++){ 
 percentX = i / (n-1);
 mathX = percentX * (xMax - xMin) + xMin;
 mathY = evaluateMathExpr(mathX);
 percentY = (mathY - yMin) / (yMax -yMin);

//Flip to match canvas coordinate.
percentY = 1 - percentY;

 xPixel = percentX * canvas.width;
 yPixel = percentY * canvas.height;
 c.lineTo(xPixel,yPixel);
 }
c.strokeStyle=color;
c.stroke();

}

function evaluateMathExpr(mathX){
    scope.x = mathX;
    //Did user enter a valid euqation?
    if (scope.x == "-12") {
      document.getElementById('inputFieldTitle').innerHTML="Invalid equation! Click need help button.";
    } else{
      document.getElementById('inputFieldTitle').innerHTML="Input equation below. Then press enter.";
    }
    scope.t = time;
  return tree.eval();
}

function textField(){
 // Make a jQuery selection to access the input DOM element.
      var input = $('#inputField');
      // Set the initial text value programmatically using jQuery.
      input.val(expr);
      
      // Listen for changes using jQuery.
      input.keypress(function (event) {
        if (event.which == 13) {
        expr = input.val();
        //Did user leave inputTextbox empty/enter numbers only?
        if(expr == ""){
          document.getElementById('inputFieldTitle').innerHTML="Input equation below. Then press enter.";
        }
        else if (!isNaN(expr)) {
          window.alert("Invalid equation! Click need help button.");
        }        
        tree = math.parse(expr, scope);                     
        drawCurve();
      }
      });
}

function startAnimation(){
  (function animloop(){
    requestAnimationFrame(animloop);
    render();
  }());
}

//crazy cool function:
// sin(x*t + t) * sin(x/3)*10

function render(){
  //increment time
  time += timeIncrement;
  //redraw
  drawCurve();
}